package main

import (
	commonpb "git.begroup.team/platform-core/be-central-proto/common"
	"git.begroup.team/platform-transport/c5/internal/services"
	"honnef.co/go/tools/config"
)

func registerBaseService(cfg *config.Config) commonpb.BaseServer {
	return services.NewBase(cfg)
}
