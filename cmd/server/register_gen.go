// IM AUTO GENERATED, BUT CAN BE OVERRIDDEN

package main

import (
	"git.begroup.team/platform-transport/c5/internal/services"
	"git.begroup.team/platform-transport/c5/internal/stores"
	"git.begroup.team/platform-transport/c5/pb"
	"honnef.co/go/tools/config"
)

func registerService(cfg *config.Config) pb.C5Server {

	mainStore := stores.NewMainStore()

	return services.New(cfg, mainStore)
}
