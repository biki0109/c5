package client

import (
	"context"

	"git.begroup.team/platform-core/kitchen/l"
	"git.begroup.team/platform-transport/c5/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/balancer/roundrobin"
)

type C5Client struct {
	pb.C5Client
}

func NewC5Client(address string) *C5Client {
	conn, err := grpc.DialContext(context.Background(), address,
		grpc.WithInsecure(),
		grpc.WithBalancerName(roundrobin.Name),
	)

	if err != nil {
		ll.Fatal("Failed to dial C5 service", l.Error(err))
	}

	c := pb.NewC5Client(conn)

	return &C5Client{c}
}
